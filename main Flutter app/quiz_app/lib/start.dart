import 'package:flutter/material.dart';
import 'package:quiz_app/main.dart';

class StartPage extends StatefulWidget {
  const StartPage({super.key});
  State createState() => _StartPage();
}

class _StartPage extends State {
  bool isSecurePassword = true;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _PasswordController = TextEditingController();
  void _login(BuildContext context) {
    String email = _emailController.text;
    String password = _PasswordController.text;

    const String ValidEmail = "harshal@gmail.com";
    const String ValidPassword = "Harshal";

    if (email == ValidEmail && password == ValidPassword) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => const QuizApp()));
    } else {
      // Show error message for invalid credentials
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: Text('Error'),
            content: Text('Invalid email or password.'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text('OK'),
              ),
            ]),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff794a4),
        title: const Text("Quiz App",
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
      ),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("lib/assets/im3.jpg"), fit: BoxFit.fill),
            gradient:
                LinearGradient(colors: [Color(0xffa8edea), Color(0xfffed6e3)])),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 80,
                width: 80,
                decoration: const BoxDecoration(
                    image:
                        DecorationImage(image: AssetImage('lib/assets/h1.jpg')),
                    borderRadius: BorderRadius.all(Radius.circular(70))),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: 350,
                height: 53,
                child: TextField(
                    controller: _emailController,
                    decoration: const InputDecoration(
                        labelText: 'Email Address',
                        iconColor: Colors.white,
                        suffixIcon: Icon(Icons.email),
                        filled: true,
                        fillColor: Colors.white)),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                width: 350,
                height: 53,
                child: TextField(
                  controller: _PasswordController,
                  obscureText: isSecurePassword,
                  decoration: InputDecoration(
                      labelText: "Password",
                      suffixIcon: togglePassword(),
                      filled: true,
                      fillColor: Colors.white),
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                  onPressed: () => _login(context),
                  child: const Text(
                    "Login",
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget togglePassword() {
    return IconButton(
        onPressed: () {
          setState(() {
            isSecurePassword = !isSecurePassword;
          });
        },
        icon: isSecurePassword
            ? Icon(Icons.visibility)
            : Icon(Icons.visibility_off),
        color: Color.fromARGB(255, 57, 49, 49));
  }
}
