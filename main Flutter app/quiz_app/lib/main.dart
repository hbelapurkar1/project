import 'package:flutter/material.dart';
import 'package:quiz_app/splash.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: SplachScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});
  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State {
  List<Map> allQuestions = [
    {
      "question": "First prime minister of india?",
      "options": ["Mango", "Jeff Bezos", "Nehru", "Elon Musk"],
      "answerIndex": 2,
    },
    {
      "question": "National Fruit of india?",
      "options": ["Mango", "Jeff Bezos", "Nehru", "Elon Musk"],
      "answerIndex": 0,
    },
    {
      "question": "Who is the founder of Amazon?",
      "options": ["Mango", "Jeff Bezos", "Nehru", "Elon Musk"],
      "answerIndex": 1,
    },
    {
      "question": "Who is the God of Cricket?",
      "options": ["Mango", "Jeff Bezos", "Nehru", "Tendulkar"],
      "answerIndex": 3,
    },
    {
      "question": "Who is the founder of Google?",
      "options": ["Steve Jobs", "Lary Page", "Bill Gates", "Elon Musk"],
      "answerIndex": 1,
    },
  ];
  bool questionScreen = true;
  int questionIndex = 0;
  int selectedAnswerIndex = -1;
  int noOfCorrectAnswers = 0;
  MaterialStateProperty<Color?> checkAnswer(int buttonIndex) {
    if (selectedAnswerIndex != -1) {
      if (buttonIndex == allQuestions[questionIndex]["answerIndex"]) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedAnswerIndex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  void validateCurrentPage() {
    if (selectedAnswerIndex == -1) {
      return;
    }
    if (selectedAnswerIndex == allQuestions[questionIndex]["answerIndex"]) {
      noOfCorrectAnswers += 1;
    }
    if (selectedAnswerIndex != -1) {
      if (questionIndex == allQuestions.length - 1) {
        setState(() {
          questionScreen = false;
        });
      }
      selectedAnswerIndex = -1;
      setState(() {
        questionIndex += 1;
      });
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff794a4),
          title: const Text("Quiz App",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
        ),
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("lib/assets/quizback.jpg")),
              gradient: LinearGradient(
                  colors: [Color(0xffa8edea), Color(0xfffed6e3)])),
          child: Center(
            child: Column(
              children: [
                const SizedBox(
                  height: 30,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  const Text(
                    "Questions : ",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    "${questionIndex + 1}/${allQuestions.length}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ]),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 100,
                  width: 100,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('lib/assets/h1.jpg')),
                      borderRadius: BorderRadius.all(Radius.circular(70))),
                ),
                Container(
                    width: 350,
                    height: 70,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          allQuestions[questionIndex]["question"],
                          style: const TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    )),
                Container(
                  height: 40,
                  width: 200,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(0),
                    ),
                    onPressed: () {
                      if (selectedAnswerIndex == -1) {
                        setState(() {
                          selectedAnswerIndex = 0;
                        });
                      }
                    },
                    child: Text(
                      "A.${allQuestions[questionIndex]["options"][0]}",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 40,
                  width: 200,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(1),
                    ),
                    onPressed: () {
                      if (selectedAnswerIndex == -1) {
                        setState(() {
                          selectedAnswerIndex = 1;
                        });
                      }
                    },
                    child: Text(
                      "B.${allQuestions[questionIndex]["options"][1]}",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 40,
                  width: 200,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(2),
                    ),
                    onPressed: () {
                      if (selectedAnswerIndex == -1) {
                        setState(() {
                          selectedAnswerIndex = 2;
                        });
                      }
                    },
                    child: Text(
                      "C.${allQuestions[questionIndex]["options"][2]}",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 40,
                  width: 200,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(3),
                    ),
                    onPressed: () {
                      if (selectedAnswerIndex == -1) {
                        setState(() {
                          selectedAnswerIndex = 3;
                        });
                      }
                    },
                    child: Text(
                      "D.${allQuestions[questionIndex]["options"][3]}",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            validateCurrentPage();
          },
          backgroundColor: Color(0xffd1fdff),
          child: const Icon(
            Icons.forward,
            color: Colors.grey,
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('lib/assets/h77.jpg'), fit: BoxFit.fill)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Container(
              //   width: double.infinity,
              //   child: Padding(
              //     padding: const EdgeInsets.only(
              //         top: 150, right: 120, left: 120, bottom: 20),
              //     child: Container(
              //         height: 170,
              //         decoration: BoxDecoration(
              //           borderRadius: BorderRadius.circular(5000),
              //           boxShadow: const [
              //             BoxShadow(
              //               offset: Offset(10, 10),
              //               color: Colors.grey,
              //               blurRadius: 10,
              //             )
              //           ],
              //           image: const DecorationImage(
              //             image: AssetImage('lib/assets/h4.jpg'),
              //             fit: BoxFit.fill,
              //           ),
              //         )),
              //   ),
              // ),
              Container(
                child: const Text(
                  "Congratulations!!!",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 50, right: 120, left: 120, bottom: 20),
                  child: Container(
                      height: 170,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5000),
                        boxShadow: const [
                          BoxShadow(
                            offset: Offset(10, 10),
                            color: Colors.grey,
                            blurRadius: 10,
                          )
                        ],
                        image: const DecorationImage(
                          image: AssetImage('lib/assets/h4.jpg'),
                          fit: BoxFit.fill,
                        ),
                      )),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "You have completed the Quiz",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromARGB(255, 84, 51, 3),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                "$noOfCorrectAnswers/${allQuestions.length}",
                style:
                    const TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
              ),
              ElevatedButton(
                  onPressed: () {
                    questionIndex = 0;
                    questionScreen = true;
                    noOfCorrectAnswers = 0;
                    selectedAnswerIndex = -1;
                    setState(() {});
                  },
                  child: const Text(
                    "Restart",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Color.fromARGB(255, 58, 61, 62),
                    ),
                  ))
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
